package ru.inno.stc14.lifecycle;

public interface ProfilingControllerMBean {
    void setEnabled(boolean enabled);
}
