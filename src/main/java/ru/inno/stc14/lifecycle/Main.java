package ru.inno.stc14.lifecycle;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "spring.xml"
        );

        Scanner scanner = new Scanner(System.in);
        while (!"q".equals(scanner.next())) {
            Example example = context.getBean(Example.class);
            example.doSome();
        }
    }
}
