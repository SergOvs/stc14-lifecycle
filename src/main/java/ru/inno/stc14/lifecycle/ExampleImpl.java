package ru.inno.stc14.lifecycle;

import javax.annotation.PostConstruct;

@Profiling
public class ExampleImpl implements Example {

    @RandomInt(min=3, max=8)
    private int length;

    @RandomInt(min=1, max=9)
    private int min;

    private String message = "Hello ";

    ExampleImpl() {
        System.out.println("constructor: " + length);
    }

    @PostConstruct
    public void init() {
        System.out.println("init: " + length);
    }

    public void doSome() {
        System.out.println("length = " + length);
        for (int i = 0; i < length; i++) {
            System.out.println(message + i);
        }
    }
}
